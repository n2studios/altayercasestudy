package com.nikinci.altayerstudycase.extensions

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment
import com.nikinci.altayerstudycase.ui.dialog.ATLoadingDialog

fun Activity.hideKeyboard() {
    var view = this.currentFocus
    if (view == null) {
        view = View(this)
    }
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.showLoading(): Dialog {

    return ATLoadingDialog.newInstance(this).also { it.show() }
}

fun AppCompatActivity.isFragmentAdded(fragment: BaseFragment): Boolean {
    val findFragmentByTag = supportFragmentManager.findFragmentByTag(fragment.getFragmentTag())
    findFragmentByTag?.let {
        return true
    }
    return false
}


fun AppCompatActivity.addFragment(containerViewId: Int, fragment: BaseFragment, addToBackStack: Boolean = false) {

    supportFragmentManager.transact {
        add(containerViewId, fragment, fragment.getFragmentTag())
        if (addToBackStack) {
            addToBackStack(fragment.getFragmentTag())
        }
    }

}


fun AppCompatActivity.replaceFragment(containerViewId: Int, fragment: BaseFragment, addToBackStack: Boolean = false, enterAnim: Int = 0, exitAnim: Int = 0) {


    supportFragmentManager.transact {
        if (addToBackStack) {
            addToBackStack(fragment.getFragmentTag())
        }
        setCustomAnimations(enterAnim, exitAnim)
        replace(containerViewId, fragment, fragment.getFragmentTag())
    }

}

fun AppCompatActivity.showFragment(fragment: BaseFragment, enterAnim: Int = 0, exitAnim: Int = 0) {

    supportFragmentManager.transact {
        setCustomAnimations(enterAnim, exitAnim)
        show(fragment)
    }
}


fun AppCompatActivity.removeFragment(fragment: BaseFragment, enterAnim: Int, exitAnim: Int) {


    supportFragmentManager.transact {
        setCustomAnimations(enterAnim, exitAnim)
        remove(fragment)
    }
}


fun AppCompatActivity.hideFragment(fragment: BaseFragment, enterAnim: Int = 0, exitAnim: Int = 0) {

    supportFragmentManager.transact {
        setCustomAnimations(enterAnim, exitAnim)
        hide(fragment)
    }

}


private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

fun Activity.showToast(message: CharSequence) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

