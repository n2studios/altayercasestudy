package com.nikinci.altayerstudycase.ui.productDetail

import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.network.NetworkConstants
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment
import kotlinx.android.synthetic.main.fragment_item_product_detail_image_pager.*


class ProductDetailImagePagerItemFragment : BaseFragment() {

    private var imageSrc: String? = null
    private var pagerPosition: Int = -1


    companion object {
        const val BUNDLE_KEY_IMAGE_URL = "bundle.key.image"
        const val BUNDLE_KEY_POSITION = "bundle.key.position"

        fun newInstance(imageSrc: String, position: Int): ProductDetailImagePagerItemFragment {

            return ProductDetailImagePagerItemFragment().apply {
                val args = Bundle()
                args.putString(BUNDLE_KEY_IMAGE_URL, imageSrc)
                args.putInt(BUNDLE_KEY_POSITION, position)
                arguments = args
            }
        }
    }


    override fun populateUI(rootView: View) {
        arguments?.run {
            imageSrc = getString(BUNDLE_KEY_IMAGE_URL)
            pagerPosition = getInt(BUNDLE_KEY_POSITION, Int.MAX_VALUE)
        }
        if (pagerPosition == 0) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageViewPagerItem.transitionName = getString(R.string.transition_product_image)
            }
        }
        (imageViewLoading.drawable as AnimationDrawable).start()
        imageSrc?.run {


            Glide.with(this@ProductDetailImagePagerItemFragment).load(NetworkConstants.BASE_URL_IMAGE_FOR_DETAIL + imageSrc).listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {

                    return false
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    imageViewLoading.visibility = View.GONE
                    return false
                }

            }).into(imageViewPagerItem)
        }


    }

    override fun getContentLayoutResId(): Int {
        return R.layout.fragment_item_product_detail_image_pager
    }
}