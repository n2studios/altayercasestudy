package com.nikinci.altayerstudycase.ui.base.views

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.nikinci.altayerstudycase.extensions.showLoading
import java.util.ArrayList

abstract class BaseFragment : Fragment() {

    var isActive: Boolean = false
        get() = isAdded
    private var loadingDialog: Dialog? = null


    protected abstract fun populateUI(rootView: View)

    open fun getFragmentTag(): String {
        return javaClass.simpleName
    }

    open fun getScreenName(): String {
        return javaClass.simpleName
    }

    @LayoutRes
    protected abstract fun getContentLayoutResId(): Int


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(getContentLayoutResId(), container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateUI(view)
    }


    open fun setLoadingIndicator(active: Boolean) {
        if (active) {
            loadingDialog = activity?.showLoading()
        } else {
            loadingDialog?.dismiss()
        }

    }


}