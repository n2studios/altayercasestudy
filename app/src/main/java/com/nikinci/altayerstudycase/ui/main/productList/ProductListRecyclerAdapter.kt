package com.nikinci.altayerstudycase.ui.main.productList

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.network.NetworkConstants
import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.ui.productDetail.ProductDetailActivity
import com.nikinci.altayerstudycase.util.ColorUtil
import com.nikinci.altayerstudycase.util.ItemOffsetDecoration
import kotlinx.android.synthetic.main.item_product_list.view.*
import kotlinx.android.synthetic.main.item_productlist_header.view.*
import kotlinx.android.synthetic.main.item_recycler_more_loading.view.*


class ProductListRecyclerAdapter(val productList: List<Model.Product>, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ITEMTYPE_PRODUCT = 0
    private val ITEMTYPE_LOADING = 1
    private val ITEMTYPE_HEADER = 2
    var smartFilters: Model.SmartFilters = Model.SmartFilters()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        return when (viewType) {
            ITEMTYPE_HEADER -> {
                val itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_productlist_header, parent, false)
                HeaderViewHolder(itemView)
            }
            ITEMTYPE_LOADING -> {
                val itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_recycler_more_loading, parent, false)
                LoadingViewHolder(itemView)
            }
            else -> {
                val itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_product_list, parent, false)
                ProductViewHolder(itemView)

            }
        }


    }

    override fun getItemCount(): Int {
        return productList.size + 1
    }

    private fun getItem(position: Int): Model.Product {
        return productList[position - 1]
    }


    override fun getItemViewType(position: Int): Int {

        return when {
            position == 0 -> ITEMTYPE_HEADER
            getItem(position).productId == Model.TEMP_PRODUCT_ID -> ITEMTYPE_LOADING
            else -> ITEMTYPE_PRODUCT
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        if (holder is ProductViewHolder) {
            val product = getItem(position)
            holder.apply {
                Glide.with(context).load("""${NetworkConstants.BASE_URL_IMAGE_FOR_LIST}${product.image}""").into(imageViewProduct)
                textViewTitle.text = product.designerCategoryName
                textViewDescription.text = product.name
                //todo fix static text
                textViewPrice.text = StringBuilder().append(context.getString(R.string.default_price_currency)).append(" ").append(product.price)
                linearLayoutRoot.setOnClickListener {


                    context?.let {

                        var transitionActivityOptions: ActivityOptions? = null
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            val pairedImage = android.util.Pair.create<View, String>(imageViewProduct, context.getString(R.string.transition_product_image))
                            transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(it as Activity, pairedImage)
                        }
                        it.startActivity(ProductDetailActivity.newIntent(context, product), transitionActivityOptions?.toBundle())
                    }


                }

                linearLayoutColorOptions.removeAllViews()
                for (confItem: Model.ConfigurableAttribute in product.configurableAttributes) {
                    when (confItem.code) {
                        Model.ConfigurableCode.COLOR -> {
                            for (colorOption: Model.ConfigurableOption in confItem.options) {
                                val gd = GradientDrawable()
                                gd.setColor(Color.parseColor(ColorUtil.getColorHex(colorOption.label)))
                                gd.cornerRadius = 40f
                                gd.setStroke(1, Color.BLACK)
                                val imageView = ImageView(context)
                                val layoutParams = LinearLayout.LayoutParams(30, 30)
                                layoutParams.marginEnd = 16
                                imageView.layoutParams = layoutParams
                                imageView.background = gd
                                linearLayoutColorOptions.addView(imageView)


                            }
                        }
                    }
                }


            }
        } else if (holder is HeaderViewHolder) {

            holder.apply {
                val smartFilterRecyclerAdapter = SmartFilterRecyclerAdapter(smartFilters.selectable, context)
                val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                recyclerviewSmartFilters.layoutManager = layoutManager
                recyclerviewSmartFilters.adapter = smartFilterRecyclerAdapter
                if (recyclerviewSmartFilters.itemDecorationCount < 1) {
                    recyclerviewSmartFilters.addItemDecoration(ItemOffsetDecoration(context.resources.getDimension(R.dimen.app_padding_4).toInt()))
                }
            }

        } else if (holder is LoadingViewHolder) {
            (holder.imageViewProgressBar.drawable as AnimationDrawable).start()
        }

    }


    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewProduct = itemView.imageViewProduct
        val textViewTitle = itemView.textViewTitle
        val textViewDescription = itemView.textViewDescription
        val textViewPrice = itemView.textViewPrice
        val linearLayoutRoot = itemView.linearLayoutRoot
        val linearLayoutColorOptions = itemView.linearLayoutColorOptions

    }

    internal class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewProgressBar = itemView.imageViewProgressBar

    }


    internal class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recyclerviewSmartFilters: RecyclerView = itemView.recyclerviewSmartFilters
    }
}