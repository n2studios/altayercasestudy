package com.nikinci.altayerstudycase.ui.dialog

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import kotlinx.android.synthetic.main.layout_loading.*
import com.nikinci.altayerstudycase.R

class ATLoadingDialog(context: Context) : ATBaseDialog(context) {


    companion object {
        fun newInstance(context: Context): ATLoadingDialog {

            return ATLoadingDialog(context)
        }
    }

    override fun populateUi() {

        (imageViewProgress.drawable as AnimationDrawable).start()
    }

    override fun getLayoutRes(): Int {
        return R.layout.layout_loading
    }

}