package com.nikinci.altayerstudycase.ui.productDetail

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.nikinci.altayerstudycase.network.data.Model

class ProductDetailViewModel : ViewModel() {

    private val currentProduct = MutableLiveData<Model.Product>()
    private val selectedSizeOption = MutableLiveData<Model.ConfigurableOption>()
    private val selectedColorOption = MutableLiveData<Model.ConfigurableOption>()


    fun setSelectedColorOption(colorOption: Model.ConfigurableOption) {
        selectedColorOption.value = colorOption
    }

    fun getSelectedColorOption(): LiveData<Model.ConfigurableOption> {
        return selectedColorOption
    }


    fun setSelectedSizeOption(sizeOption: Model.ConfigurableOption?) {
        selectedSizeOption.value = sizeOption
    }

    fun getSelectedSizeOption(): LiveData<Model.ConfigurableOption> {
        return selectedSizeOption
    }

    fun setProduct(product: Model.Product) {
        currentProduct.value = product
    }

    fun getCurrentProduct(): LiveData<Model.Product> {
        return currentProduct
    }

    companion object {
        fun create(activity: FragmentActivity): ProductDetailViewModel {
            return ViewModelProviders.of(activity).get(ProductDetailViewModel::class.java)
        }
    }


}