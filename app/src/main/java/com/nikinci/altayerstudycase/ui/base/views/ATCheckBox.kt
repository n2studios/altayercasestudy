package com.nikinci.altayerstudycase.ui.base.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox
import com.nikinci.altayerstudycase.R

class ATCheckBox @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.atCheckBoxStyle
) : AppCompatCheckBox(context, attrs, defStyleAttr)