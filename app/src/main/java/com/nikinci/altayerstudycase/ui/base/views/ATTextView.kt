package com.nikinci.altayerstudycase.ui.base.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.nikinci.altayerstudycase.R


class ATTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.atTextViewStyle
) : AppCompatTextView(context, attrs, defStyleAttr)