package com.nikinci.altayerstudycase.ui.main.bag

import android.os.Bundle
import com.nikinci.altayerstudycase.R
import android.view.View
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment

class BagFragment : BaseFragment() {


    companion object {
        fun newInstance(): BagFragment {

            val args = Bundle()
            return BagFragment().apply {
                arguments = args
            }
        }
    }


    override fun populateUI(rootView: View) {
    }

    override fun getContentLayoutResId(): Int {
        return R.layout.fragment_bag
    }
}