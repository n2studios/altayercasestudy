package com.nikinci.altayerstudycase.ui.productDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.extensions.getHtmlString
import com.nikinci.altayerstudycase.network.data.Model
import kotlinx.android.synthetic.main.item_attribute.view.*


class ProductDetailAttributesRecyclerAdapter(val attributeList: List<Model.Attribute>, val context: Context) : RecyclerView.Adapter<ProductDetailAttributesRecyclerAdapter.ViewHolderAttribute>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductDetailAttributesRecyclerAdapter.ViewHolderAttribute {


        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_attribute, parent, false)
        return ProductDetailAttributesRecyclerAdapter.ViewHolderAttribute(itemView)

    }


    override fun getItemCount(): Int {
        return attributeList.size
    }

    private fun getItem(position: Int): Model.Attribute {
        return attributeList[position]
    }


    override fun onBindViewHolder(holder: ProductDetailAttributesRecyclerAdapter.ViewHolderAttribute, position: Int) {

        holder.apply {

            val item = getItem(position)
            textViewTitle.text = item.name
            textViewDescription.text = item.value.getHtmlString()
            linearLayoutRoot.setOnClickListener {
                if (textViewDescription.maxLines < 2) {
                    textViewDescription.maxLines = Int.MAX_VALUE
                    textViewTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_top, 0)
                    linearLayoutRoot.setBackgroundColor(ContextCompat.getColor(context, R.color.c_black_20))
                } else {
                    textViewDescription.maxLines = 1
                    linearLayoutRoot.setBackgroundColor(ContextCompat.getColor(context, R.color.c_white))
                    textViewTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down_black_24dp, 0)
                }
            }

            if (position == attributeList.size - 1) {
                dividerEnd.visibility = View.VISIBLE
            } else {
                dividerEnd.visibility = View.GONE
            }
        }


    }


    class ViewHolderAttribute(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dividerEnd = itemView.dividerEnd
        val linearLayoutRoot = itemView.linearLayoutRoot
        val textViewTitle = itemView.textViewTitle
        val textViewDescription = itemView.textViewDescription
    }

}