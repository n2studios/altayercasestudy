package com.nikinci.altayerstudycase.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.extensions.addFragment
import com.nikinci.altayerstudycase.extensions.hideFragment
import com.nikinci.altayerstudycase.extensions.isFragmentAdded
import com.nikinci.altayerstudycase.extensions.showFragment
import com.nikinci.altayerstudycase.network.Injection
import com.nikinci.altayerstudycase.ui.base.views.BaseActivity
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment
import com.nikinci.altayerstudycase.ui.main.account.AccountFragment
import com.nikinci.altayerstudycase.ui.main.bag.BagFragment
import com.nikinci.altayerstudycase.ui.main.brands.BrandsFragment
import com.nikinci.altayerstudycase.ui.main.home.HomeFragment
import com.nikinci.altayerstudycase.ui.main.productList.ProductListFragment
import com.nikinci.altayerstudycase.ui.main.productList.ProductListPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private var productListFragment: ProductListFragment? = null
    private var activeFragment: BaseFragment? = null
    private var homeFragment: BaseFragment? = null
    private var brandsFragment: BaseFragment? = null
    private var bagFragment: BaseFragment? = null
    private var accountFragment: BaseFragment? = null

    override fun populateUI(savedInstanceState: Bundle?) {
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_category
    }

    override fun getContentViewLayoutResId(): Int {
        return R.layout.activity_main
    }

    override fun getFragmentContainerId(): Int {
        return R.id.frame_layout_container
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        var fragment: BaseFragment? = null
        when (item.itemId) {
            R.id.navigation_home -> {
                this.homeFragment?.let {
                    fragment = it
                } ?: kotlin.run {
                    this.homeFragment = HomeFragment.newInstance()
                    fragment = homeFragment
                }

            }
            R.id.navigation_category -> {
                productListFragment?.let {
                    fragment = it
                } ?: kotlin.run {
                    productListFragment = ProductListFragment.newInstance().apply {
                        ProductListPresenter(Injection.provideProductListRepository(applicationContext), this)
                        fragment = this
                    }
                }
            }
            R.id.navigation_brand -> {
                brandsFragment?.let {
                    fragment = it
                } ?: kotlin.run {
                    brandsFragment = BrandsFragment.newInstance()
                    fragment = brandsFragment
                }
            }
            R.id.navigation_bag -> {
                this.bagFragment?.let {
                    fragment = it
                } ?: kotlin.run {
                    bagFragment = BagFragment.newInstance()
                    fragment = bagFragment
                }
            }
            R.id.navigation_account -> {
                accountFragment?.let {
                    fragment = it
                } ?: kotlin.run {
                    accountFragment = AccountFragment.newInstance()
                    fragment = accountFragment
                }
            }
        }

        activeFragment?.let {
            hideFragment(it)
        }

        fragment?.let {
            if (isFragmentAdded(it)) {
                showFragment(it)
            } else {
                addFragment(getFragmentContainerId(), it)
            }
            activeFragment = it
        }


        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)

        }

    }
}
