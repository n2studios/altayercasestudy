package com.nikinci.altayerstudycase.ui.main.account

import android.os.Bundle
import com.nikinci.altayerstudycase.R
import android.view.View
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment

class AccountFragment : BaseFragment() {


    companion object {
        fun newInstance(): AccountFragment {

            val args = Bundle()
            return AccountFragment().apply {
                arguments = args
            }
        }
    }


    override fun populateUI(rootView: View) {
    }

    override fun getContentLayoutResId(): Int {
        return R.layout.fragment_account
    }
}