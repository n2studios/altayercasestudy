package com.nikinci.altayerstudycase.ui.main.productList

import com.nikinci.altayerstudycase.network.data.AppDatas
import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.network.data.source.ProductDataSource
import com.nikinci.altayerstudycase.network.data.source.ProductRepository

class ProductListPresenter(val producListRepository: ProductRepository, val productListView: ProductListContract.View)
    : ProductListContract.Presenter {

    override fun start() {
        currentPage = 0
        productListView.resetProductList()
        getMoreProductList(true)

    }

    var isLoadingMore: Boolean = false

    init {
        productListView.presenter = this
    }

    override var currentPage: Int = 0


    override fun getMoreProductList(forStart: Boolean) {
        if (!isLoadingMore) {
            isLoadingMore = true
            if (!forStart) {
                productListView.setMoreLoadingIndicator(true)
            } else {
                productListView.setSwipeLoadingIndicator(true)
            }
            producListRepository.getProductList(currentPage, object : ProductDataSource.LoadProductListCallback {
                override fun onProductListRetrieved(productListResponse: Model.ProductListResponse) {
                    isLoadingMore = false
                    if (!forStart) {
                        productListView.setMoreLoadingIndicator(false)
                    } else {
                        productListView.setSwipeLoadingIndicator(false)
                        productListView.setSmartFilters(productListResponse.smartFilters)
                    }
                    productListView.showProductList(productListResponse.hits)
                    productListResponse.facets?.color?.run {
                        AppDatas.getInstance().colorList = this.value
                    }
                    currentPage++
                }

                override fun onDataNotAvailable() {
                    isLoadingMore = false
                    if (!forStart) {
                        productListView.setMoreLoadingIndicator(false)
                    } else {
                        productListView.setSwipeLoadingIndicator(false)
                    }
                    productListView.showNoMoreProduct()
                }
            })
        }
    }

    override fun cancelAllRequests() {
    }


}