/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nikinci.altayerstudycase.ui.main.productList

import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.ui.base.BasePresenter
import com.nikinci.altayerstudycase.ui.base.BaseView


/**
 * This specifies the contract between the view and the presenter.
 */
interface ProductListContract {

    interface View : BaseView<Presenter> {

        fun showProductList(productList: List<Model.Product>)

        fun setSmartFilters(smartFilters: Model.SmartFilters)

        fun showNoMoreProduct()

        fun resetProductList()

        fun setMoreLoadingIndicator(active: Boolean)

        fun setSwipeLoadingIndicator(active: Boolean)

    }

    interface Presenter : BasePresenter {

        var currentPage: Int

        fun getMoreProductList(forStart: Boolean = false)

        fun start()

    }
}
