package com.nikinci.altayerstudycase.ui.productDetail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.nikinci.altayerstudycase.network.data.Model

class ProductDetailImagePagerAdapter(fm: FragmentManager, private val mediaItemList: List<Model.MediaItem>) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int {
        return mediaItemList.size
    }

    override fun getItem(position: Int): Fragment {
        return ProductDetailImagePagerItemFragment.newInstance(mediaItemList[position].src, position)
    }
}
