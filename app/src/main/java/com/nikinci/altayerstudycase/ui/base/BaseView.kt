package com.nikinci.altayerstudycase.ui.base

interface BaseView<T> {

    var presenter: T

    fun setLoadingIndicator(active: Boolean)

    val isActive: Boolean
}