package com.nikinci.altayerstudycase.ui.main.productList

import android.os.Bundle
import com.nikinci.altayerstudycase.R
import android.view.View
import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment
import kotlinx.android.synthetic.main.fragment_productlist.*
import androidx.recyclerview.widget.GridLayoutManager
import com.nikinci.altayerstudycase.util.EndlessRecyclerViewScrollListener
import com.nikinci.altayerstudycase.util.ItemOffsetDecoration


class ProductListFragment : BaseFragment(), ProductListContract.View {


    private val mProductList = mutableListOf<Model.Product>()
    private lateinit var mAdapter: ProductListRecyclerAdapter


    override lateinit var presenter: ProductListContract.Presenter


    companion object {
        fun newInstance(): ProductListFragment {

            val args = Bundle()
            return ProductListFragment().apply {
                arguments = args
            }
        }
    }

    override fun setSmartFilters(smartFilters: Model.SmartFilters) {
        mAdapter.smartFilters = smartFilters
    }

    override fun setSwipeLoadingIndicator(active: Boolean) {
        swipeRefresh.isRefreshing = active
    }

    override fun setMoreLoadingIndicator(active: Boolean) {
        if (active) {
            val tempProduct = Model.Product()
            mProductList.add(tempProduct)
            mAdapter.notifyItemInserted(mProductList.size - 1)
        } else {
            mProductList.removeAt(mProductList.size - 1)
            mAdapter.notifyItemRemoved(mProductList.size - 1)
        }
    }

    override fun resetProductList() {
        mProductList.clear()
        mAdapter.notifyDataSetChanged()
    }


    override fun showProductList(productList: List<Model.Product>) {
        if (swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }
        mProductList.addAll(productList)
        mAdapter.notifyDataSetChanged()
    }

    override fun showNoMoreProduct() {
    }

    override fun populateUI(rootView: View) {

        swipeRefresh.setOnRefreshListener {
            presenter.start()
        }
        val gridLayoutManager = GridLayoutManager(context, 2)
        recyclerview.layoutManager = gridLayoutManager
        recyclerview.addItemDecoration(ItemOffsetDecoration(resources.getDimension(R.dimen.app_padding_8).toInt()))
        context?.let {
            mAdapter = ProductListRecyclerAdapter(mProductList, it)
            recyclerview.adapter = mAdapter
        }
        presenter.start()

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {

            override fun getSpanSize(position: Int): Int {
                return when {
                    position == 0 -> 2
                    mProductList[position-1].productId == Model.TEMP_PRODUCT_ID -> 2
                    else -> 1
                }

            }
        }
        recyclerview.addOnScrollListener(object : EndlessRecyclerViewScrollListener(gridLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                presenter.getMoreProductList()

            }
        })
    }


    override fun getContentLayoutResId(): Int {
        return R.layout.fragment_productlist
    }
}