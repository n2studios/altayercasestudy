package com.nikinci.altayerstudycase.ui.base.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.nikinci.altayerstudycase.R

class ATEditText @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.atEditTextStyle
) : AppCompatEditText(context, attrs, defStyleAttr)