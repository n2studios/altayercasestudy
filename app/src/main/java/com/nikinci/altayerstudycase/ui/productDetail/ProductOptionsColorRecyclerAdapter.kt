package com.nikinci.altayerstudycase.ui.productDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.network.NetworkConstants
import com.nikinci.altayerstudycase.network.data.Model
import kotlinx.android.synthetic.main.item_options_color.view.*


class ProductOptionsColorRecyclerAdapter(val confOptionsList: List<Model.ConfigurableOption>, val context: Context) : RecyclerView.Adapter<ProductOptionsColorRecyclerAdapter.ViewHolderOptionsColor>() {


    private var selectedPosition: Int = RecyclerView.NO_POSITION
    private var productDetailViewModel: ProductDetailViewModel = ProductDetailViewModel.create(context as FragmentActivity)


    init {
        findDefaultSelectedPositon()
    }

    private fun findDefaultSelectedPositon() {
        val configurableOption = productDetailViewModel.getSelectedColorOption().value
        configurableOption?.let {
            for ((index, value) in confOptionsList.withIndex()) {
                if (value.optionId == it.optionId) {
                    selectedPosition = index
                    return
                }
            }

        } ?: kotlin.run {
            for ((index, value) in confOptionsList.withIndex()) {
                val currentProduct = productDetailViewModel.getCurrentProduct().value as Model.Product
                if (value.label.equals(currentProduct.color, false)) {
                    selectedPosition = index
                    return
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductOptionsColorRecyclerAdapter.ViewHolderOptionsColor {


        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_options_color, parent, false)
        return ProductOptionsColorRecyclerAdapter.ViewHolderOptionsColor(itemView)

    }


    override fun getItemCount(): Int {
        return confOptionsList.size
    }

    private fun getItem(position: Int): Model.ConfigurableOption {
        return confOptionsList[position]
    }


    override fun onBindViewHolder(holder: ProductOptionsColorRecyclerAdapter.ViewHolderOptionsColor, position: Int) {


        holder.apply {
            val configurableOption = getItem(position)
            textViewOptionsColor.text = configurableOption.label
            Glide.with(context).load(NetworkConstants.BASE_URL_IMAGE_FOR_DETAIL + configurableOption.attributeSpecificProperties.productThumbnail).into(imageViewOptionsColor)

            if (configurableOption.isInStock) {
                relativeLayoutNotInStock.visibility = View.GONE
            } else {
                relativeLayoutNotInStock.visibility = View.VISIBLE
            }

            if (selectedPosition == adapterPosition) {
                relativeLayoutSelected.visibility = View.VISIBLE
            } else {
                relativeLayoutSelected.visibility = View.GONE

            }

            linearLayoutRoot.setOnClickListener {
                if (configurableOption.isInStock) {
                    notifyItemChanged(selectedPosition)
                    selectedPosition = adapterPosition
                    notifyItemChanged(selectedPosition)
                    productDetailViewModel.setSelectedColorOption(configurableOption)
                }
            }
        }


    }


    class ViewHolderOptionsColor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewOptionsColor = itemView.imageViewOptionsColor
        val textViewOptionsColor = itemView.textViewOptionsColor
        val linearLayoutRoot = itemView.linearLayoutRoot
        val relativeLayoutSelected = itemView.relativeLayoutSelected
        val relativeLayoutNotInStock = itemView.relativeLayoutNotInStock
    }

}