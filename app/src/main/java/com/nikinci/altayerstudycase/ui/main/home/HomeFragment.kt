package com.nikinci.altayerstudycase.ui.main.home

import android.os.Bundle
import android.view.View
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment

class HomeFragment : BaseFragment() {


    companion object {
        fun newInstance(): HomeFragment {

            val args = Bundle()
            return HomeFragment().apply {
                arguments = args
            }
        }
    }


    override fun populateUI(rootView: View) {
    }

    override fun getContentLayoutResId(): Int {
        return R.layout.fragment_home
    }
}