package com.nikinci.altayerstudycase.ui.productDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.network.data.Model
import kotlinx.android.synthetic.main.item_options_size.view.*


class ProductOptionsSizeRecyclerAdapter(val confOptionsList: List<Model.ConfigurableOption>, val context: Context) : RecyclerView.Adapter<ProductOptionsSizeRecyclerAdapter.ViewHolderOptionsSize>() {


    private var selectedPosition: Int = RecyclerView.NO_POSITION
    private val bgSelected = ContextCompat.getDrawable(context, R.drawable.bg_options_size_selected)
    private val bgUnSelected = ContextCompat.getDrawable(context, R.drawable.bg_options_size_unselected)
    private val notInStockTextColor = ContextCompat.getColor(context, R.color.c_black_10)
    private val inStockTextColor = ContextCompat.getColor(context, R.color.c_black)

    private var productDetailViewModel: ProductDetailViewModel = ProductDetailViewModel.create(context as FragmentActivity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductOptionsSizeRecyclerAdapter.ViewHolderOptionsSize {


        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_options_size, parent, false)
        return ProductOptionsSizeRecyclerAdapter.ViewHolderOptionsSize(itemView)

    }


    override fun getItemCount(): Int {
        return confOptionsList.size
    }

    private fun getItem(position: Int): Model.ConfigurableOption {
        return confOptionsList[position]
    }


    override fun onBindViewHolder(holder: ProductOptionsSizeRecyclerAdapter.ViewHolderOptionsSize, position: Int) {


        holder.apply {
            val configurableOption = getItem(position)
            textViewOptionsSizeName.text = configurableOption.label
            if (configurableOption.isInStock) {
                textViewOptionsSizeName.setTextColor(inStockTextColor)
            } else {
                textViewOptionsSizeName.setTextColor(notInStockTextColor)

            }

            if (selectedPosition == position) {
                textViewOptionsSizeName.background = bgSelected
            } else {
                textViewOptionsSizeName.background = bgUnSelected

            }
            textViewOptionsSizeName.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    if (configurableOption.isInStock) {
                        notifyItemChanged(selectedPosition)
                        selectedPosition = adapterPosition
                        notifyItemChanged(selectedPosition)
                        productDetailViewModel.setSelectedSizeOption(configurableOption)
                    }
                }
            }
        }
    }


    class ViewHolderOptionsSize(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewOptionsSizeName = itemView.textViewOptionsSizeName
    }

}