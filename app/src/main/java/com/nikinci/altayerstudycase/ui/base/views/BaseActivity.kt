package com.nikinci.altayerstudycase.ui.base.views

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import  com.nikinci.altayerstudycase.R

abstract class BaseActivity : AppCompatActivity() {

    protected abstract fun populateUI(savedInstanceState: Bundle?)

    @LayoutRes
    protected open fun getContentViewLayoutResId(): Int {
        return R.layout.activity_base

    }

    @IdRes
    protected open fun getFragmentContainerId(): Int {
        return R.id.frame_layout_container
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentViewLayoutResId())
        populateUI(savedInstanceState)

    }



}