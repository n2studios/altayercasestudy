package com.nikinci.altayerstudycase.ui.main.brands

import android.os.Bundle
import com.nikinci.altayerstudycase.R
import android.view.View
import com.nikinci.altayerstudycase.ui.base.views.BaseFragment

class BrandsFragment : BaseFragment() {


    companion object {
        fun newInstance(): BrandsFragment {

            val args = Bundle()
            return BrandsFragment().apply {
                arguments = args
            }
        }
    }


    override fun populateUI(rootView: View) {
    }

    override fun getContentLayoutResId(): Int {
        return R.layout.fragment_brands
    }
}