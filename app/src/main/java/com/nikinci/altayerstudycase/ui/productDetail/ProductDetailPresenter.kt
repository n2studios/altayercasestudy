package com.nikinci.altayerstudycase.ui.productDetail

import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.network.data.source.ProductDataSource
import com.nikinci.altayerstudycase.network.data.source.ProductRepository

class ProductDetailPresenter(val producListRepository: ProductRepository, val productDetailView: ProductDetailContract.View)
    : ProductDetailContract.Presenter {


    override fun findBySlug(product: Model.Product) {

        producListRepository.findProductBySlug(product, object : ProductDataSource.FindProductCallback {
            override fun onProductFind(response: Model.Product) {
                productDetailView.showProductDetail(response)
            }

            override fun onProductFindFailed() {
                productDetailView.showErrorLoadingDetail()
            }
        })

    }


    init {
        productDetailView.presenter = this
    }


    override fun cancelAllRequests() {
    }



}