package com.nikinci.altayerstudycase.ui.dialog

import android.content.Context
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatDialog

import  com.nikinci.altayerstudycase.R

abstract class ATBaseDialog(context: Context, theme: Int = R.style.ATDialogTheme) : AppCompatDialog(context, theme) {

    protected abstract fun populateUi()
    @LayoutRes
    protected abstract fun getLayoutRes(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())

        setCancelable(false)
        setCanceledOnTouchOutside(false)

        populateUi()
    }


}