package com.nikinci.altayerstudycase.ui.productDetail

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.nikinci.altayerstudycase.R
import com.nikinci.altayerstudycase.extensions.showLoading
import com.nikinci.altayerstudycase.network.Injection
import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.network.data.Model.Product
import com.nikinci.altayerstudycase.ui.base.views.BaseActivity
import com.nikinci.altayerstudycase.util.ColorUtil
import com.nikinci.altayerstudycase.util.ItemOffsetDecoration
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_product_detail_content.*


class ProductDetailActivity : BaseActivity(), ProductDetailContract.View {


    override lateinit var presenter: ProductDetailContract.Presenter
    lateinit var bottomSheetBehavior: BottomSheetBehavior<RelativeLayout>
    private lateinit var productDetailViewModel: ProductDetailViewModel

    override fun setLoadingIndicator(active: Boolean) {

    }

    override val isActive: Boolean
        get() = !isFinishing


    lateinit var mProduct: Product

    companion object {
        const val BUNDLE_KEY_PRODUCT = "bundle.key.product"
        fun newIntent(context: Context, product: Product): Intent {

            return Intent(context, ProductDetailActivity::class.java).apply {
                putExtra(BUNDLE_KEY_PRODUCT, product)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun populateUI(savedInstanceState: Bundle?) {

        mProduct = intent.getSerializableExtra(BUNDLE_KEY_PRODUCT) as Product
        productDetailViewModel = ProductDetailViewModel.create(this)
        productDetailViewModel.setProduct(mProduct)
        collapsingToolbar.title = mProduct.name
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent))
        collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.c_black_54))

        ProductDetailPresenter(Injection.provideProductListRepository(applicationContext), this)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout)
        toolbar.setNavigationIcon(R.drawable.ic_back_icon)
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        setProductValues()

        (imageViewLoading.drawable as AnimationDrawable).start()
        presenter.findBySlug(mProduct)

        productDetailViewModel.getSelectedColorOption().observe(this, Observer {
            setColorOptionLabel(it.label)
            productDetailViewModel.setSelectedSizeOption(null)
            presenter.findBySlug(Product(sku = it.simpleProductSkus[0].toString()))
        })
        productDetailViewModel.getSelectedSizeOption().observe(this, Observer {
            it?.let {
                setSizeOptionLabel(it.label)
                buttonOptionsAddToBag.isClickable = true
                buttonOptionsAddToBag.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.button_bg_default))
            } ?: kotlin.run {
                buttonOptionsAddToBag.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.button_bg_inactive))
                buttonOptionsAddToBag.isClickable = false
            }
        })

        buttonOptionsAddToBag.setOnClickListener {
            val currentProduct = productDetailViewModel.getCurrentProduct().value
            val selectedSizeOption = productDetailViewModel.getSelectedSizeOption().value
            var selectedColorOption = productDetailViewModel.getSelectedColorOption().value


            val builder = StringBuilder().append("Added Bag -->  ").append(" productId: " + currentProduct?.productId).append("   SizeLabel: " + selectedSizeOption?.label).append("   SizeOptionId: " + selectedSizeOption?.optionId)

            selectedColorOption?.let {
                builder.append(" ColorLabel:  " + selectedColorOption?.label)
            } ?: kotlin.run {
                builder.append(" ColorLabel:  " + currentProduct?.color)

            }

            val loadindDialog = showLoading()
            Handler().postDelayed({
                loadindDialog?.dismiss()
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                Snackbar
                        .make(coordinatorLayout, builder, Snackbar.LENGTH_LONG).show()
            }, 1000)


        }

    }

    private fun setProductValues() {
        mProduct.apply {
            textViewTitle.text = designerCategoryName
            textViewDescription.text = name
            //todo fix static text
            textViewPrice.text = StringBuilder().append(getString(R.string.default_price_currency)).append(" ").append(price)
            textViewVatInfo.text = vatInfo
            textViewStockTrigger.text = stockTriggerText

            setOptionsClickListener(buttonAddToBag)

            //image pager
            setPagerItems(media)

        }

    }

    private fun setPagerItems(media: MutableList<Model.MediaItem>) {
        media.run {
            val imageMediaList: MutableList<Model.MediaItem> = arrayListOf()
            this.forEach { e ->
                when (e.mediaType) {
                    Model.MediaType.IMAGE -> {
                        imageMediaList.add(e)
                    }
                    else -> {
                    }
                }
                viewPagerProductImages.adapter = ProductDetailImagePagerAdapter(supportFragmentManager, imageMediaList)
                indicator.setViewPager(viewPagerProductImages)
            }
        }
    }


    override fun getContentViewLayoutResId(): Int {
        return R.layout.activity_detail
    }

    override fun showProductDetail(response: Model.Product) {
        this.mProduct = response
        productDetailViewModel.setProduct(mProduct)

        mProduct.run {
            productDetailViewModel.getCurrentProduct().value?.productId = productId
            textViewStaticInfo.text = staticInfo
            val attributesRecyclerAdapter = ProductDetailAttributesRecyclerAdapter(copyAttributes, this@ProductDetailActivity)
            recyclerviewAttributes.layoutManager = LinearLayoutManager(this@ProductDetailActivity)
            recyclerviewAttributes.adapter = attributesRecyclerAdapter
            setConfigurableAttributes(configurableAttributes)
            setColorOptionLabel(color)
            setSizeOptionLabel(sizeCode)
            textViewStockTrigger.text = stockTriggerText
            setPagerItems(media)


        }
        linearLayoutDetailContent.visibility = View.VISIBLE
        imageViewLoading.visibility = View.GONE

    }

    private fun setConfigurableAttributes(configurableAttributes: MutableList<Model.ConfigurableAttribute>) {

        for (confItem: Model.ConfigurableAttribute in configurableAttributes) {
            when (confItem.code) {
                Model.ConfigurableCode.COLOR -> {

                    val productOptionsColorRecyclerAdapter = ProductOptionsColorRecyclerAdapter(confItem.options, this)
                    val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                    recyclerviewOptionsColor.layoutManager = layoutManager
                    if (recyclerviewOptionsColor.itemDecorationCount < 1) {
                        recyclerviewOptionsColor.addItemDecoration(ItemOffsetDecoration(resources.getDimension(R.dimen.app_padding_4).toInt()))
                    }
                    recyclerviewOptionsColor.adapter = productOptionsColorRecyclerAdapter

                    linearLayoutOptionsColorRoot.visibility = View.VISIBLE
                    setOptionsClickListener(linearLayoutColorOptionRoot)

                    textViewColorOption.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down_black_24dp, 0)
                }
                Model.ConfigurableCode.SIZECODE -> {
                    val productOptionsSizeRecyclerAdapter = ProductOptionsSizeRecyclerAdapter(confItem.options, this)
                    val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                    recyclerviewOptionsSize.layoutManager = layoutManager
                    if (recyclerviewOptionsSize.itemDecorationCount < 1) {
                        recyclerviewOptionsSize.addItemDecoration(ItemOffsetDecoration(resources.getDimension(R.dimen.app_padding_4).toInt()))
                    }
                    recyclerviewOptionsSize.adapter = productOptionsSizeRecyclerAdapter


                    linearLayoutOptionsSizeRoot.visibility = View.VISIBLE
                    setOptionsClickListener(linearLayoutSizeOptionRoot)
                    textViewSizeOption.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down_black_24dp, 0)
                }

            }
        }


    }

    private fun setOptionsClickListener(view: View) {
        view.setOnClickListener {

            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED


        }
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        } else {
            super.onBackPressed()
        }
    }

    private fun setSizeOptionLabel(size: String) {
        textViewSizeOption.text = size
    }

    private fun setColorOptionLabel(color: String) {


        val gd = GradientDrawable()
        gd.setColor(Color.parseColor(ColorUtil.getColorHex(color)))
        gd.cornerRadius = 40f
        gd.setStroke(2, Color.BLACK)
        imageViewColor.background = gd
        textViewColorOption.text = color


    }

    override fun showErrorLoadingDetail() {
        //todo show error
    }
}