package com.nikinci.altayerstudycase.ui.base.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.nikinci.altayerstudycase.R


class ATButton @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.atButtonStyle
) : AppCompatButton(context, attrs, defStyleAttr)