package com.nikinci.altayerstudycase.ui.main.productList

import android.content.Context
import com.nikinci.altayerstudycase.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nikinci.altayerstudycase.network.data.Model
import kotlinx.android.synthetic.main.item_smart_filter.view.*


class SmartFilterRecyclerAdapter(val filterList: List<Model.Filter>, val context: Context) : RecyclerView.Adapter<SmartFilterRecyclerAdapter.ViewHolderSmartFilter>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SmartFilterRecyclerAdapter.ViewHolderSmartFilter {


        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_smart_filter, parent, false)
        return ViewHolderSmartFilter(itemView)

    }


    override fun getItemCount(): Int {
        return filterList.size
    }

    private fun getItem(position: Int): Model.Filter {
        return filterList[position]
    }


    override fun onBindViewHolder(holder: SmartFilterRecyclerAdapter.ViewHolderSmartFilter, position: Int) {


        holder.apply {
            textViewFilterName.text = getItem(position).name
        }


    }


    class ViewHolderSmartFilter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewFilterName = itemView.textViewFilterName
    }

}