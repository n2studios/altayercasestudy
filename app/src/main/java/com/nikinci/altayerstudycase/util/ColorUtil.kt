package com.nikinci.altayerstudycase.util

import com.nikinci.altayerstudycase.network.data.AppDatas
import com.nikinci.altayerstudycase.network.data.Model

object ColorUtil {

    fun getColorHex(label: String): String {

        getColorItem(label)?.run {
            return hex
        }

        return label
    }

    fun getColorItem(label: String): Model.ColorItem? {
        val colorList = AppDatas.getInstance().colorList
        colorList?.run {
            for (colorItem: Model.ColorItem in colorList) {
                if (colorItem.name.equals(label, true)) {
                    return colorItem
                }
            }
        }
        return null
    }
}