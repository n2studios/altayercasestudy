package com.nikinci.altayerstudycase.network

import io.reactivex.observers.DisposableObserver
import org.intellij.lang.annotations.Language
import retrofit2.Response


abstract class CallbackWrapper<T> : DisposableObserver<Response<T>>() {

    abstract fun onSuccess(t: T)

    abstract fun onFail(cause: String)


    override fun onNext(response: Response<T>) {

        if (response.isSuccessful) {
            onSuccess(response.body() as T)
        } else {
            onFail("")
        }

    }

    override fun onError(e: Throwable) {
        if (!isDisposed) {
            onFail(e.localizedMessage)
        }
    }

    override fun onComplete() {
    }
}