package com.nikinci.altayerstudycase.network.data.source

import com.nikinci.altayerstudycase.network.data.Model

class ProductRepository(
        val productListRemoteDataSource: ProductDataSource
) : ProductDataSource {

    override fun findProductBySlug(product: Model.Product, callback: ProductDataSource.FindProductCallback) {

        productListRemoteDataSource.findProductBySlug(product, object : ProductDataSource.FindProductCallback {
            override fun onProductFind(response: Model.Product) {
                callback.onProductFind(response)
            }

            override fun onProductFindFailed() {
                callback.onProductFindFailed()
            }
        })

    }

    override fun getProductList(page: Int, callback: ProductDataSource.LoadProductListCallback) {

        productListRemoteDataSource.getProductList(page, object : ProductDataSource.LoadProductListCallback {
            override fun onProductListRetrieved(productListResponse: Model.ProductListResponse) {
                callback.onProductListRetrieved(productListResponse)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }


    companion object {

        private var INSTANCE: ProductRepository? = null

        @JvmStatic
        fun getInstance(productListRemoteDataSource: ProductDataSource): ProductRepository {
            return INSTANCE ?: ProductRepository(productListRemoteDataSource)
                    .apply { INSTANCE = this }
        }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }

}