package com.nikinci.altayerstudycase.network.data

class AppDatas {


    var colorList: List<Model.ColorItem>? = null

    companion object {

        private var INSTANCE: AppDatas? = null

        @JvmStatic
        fun getInstance(): AppDatas {
            return INSTANCE ?: AppDatas()
                    .apply { INSTANCE = this }
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }


}