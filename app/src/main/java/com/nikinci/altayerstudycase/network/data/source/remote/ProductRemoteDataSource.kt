package com.nikinci.altayerstudycase.network.data.source.remote

import com.nikinci.altayerstudycase.network.ATService
import com.nikinci.altayerstudycase.network.CallbackWrapper
import com.nikinci.altayerstudycase.network.data.Model
import com.nikinci.altayerstudycase.network.data.source.ProductDataSource
import com.ttech.android.onlineislem.core.ATApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ProductRemoteDataSource : ProductDataSource {


    var apiService: ATService
    private var callProductList: Disposable? = null
    private var callFindProduct: Disposable? = null

    companion object {
        private var INSTANCE: ProductRemoteDataSource? = null

        @JvmStatic
        fun getInstance(): ProductRemoteDataSource {
            if (INSTANCE == null) {
                synchronized(ProductRemoteDataSource::javaClass) {
                    INSTANCE = ProductRemoteDataSource()
                }
            }
            return INSTANCE!!
        }

        fun clearInstance() {
            INSTANCE = null
        }
    }

    init {
        apiService = ATApplication.instance.atService
    }

    override fun getProductList(page: Int, callback: ProductDataSource.LoadProductListCallback) {

        callProductList = apiService.getClothingList(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : CallbackWrapper<Model.ProductListResponse>() {
                    override fun onSuccess(t: Model.ProductListResponse) {
                        callback.onProductListRetrieved(t)
                    }

                    override fun onFail(cause: String) {
                        callback.onDataNotAvailable()
                    }
                })

    }

    override fun findProductBySlug(product: Model.Product, callback: ProductDataSource.FindProductCallback) {
        callFindProduct = apiService.findProductBySlug(product.sku.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : CallbackWrapper<Model.Product>() {
                    override fun onSuccess(t: Model.Product) {
                        callback.onProductFind(t)
                    }

                    override fun onFail(cause: String) {
                        callback.onProductFindFailed()
                    }
                })
    }


}