package com.nikinci.altayerstudycase.network.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

object Model {
    const val TEMP_PRODUCT_ID = -1L

    data class Product(var productId: Long = TEMP_PRODUCT_ID,
                       var sku: String = "",
                       val season: String = "",
                       val description: String = "",
                       val name: String = "",
                       val atgColorCode: String = "",
                       val motherReference: String = "",
                       val slug: String = "",
                       val sizeAndFit: String = "",
                       val image: String = "",
                       val thumbnail: String = "",
                       val swatchImage: String = "",
                       val stockTriggerText: String = "",
                       val price: String = "",
                       val color: String = "",
                       val colorId: String = "",
                       val sizeCode: String = "",
                       val sizeCodeId: String = "",
                       val designerCategoryName: String = "",
                       val vatInfo: String = "",
                       val staticInfo: String = "",
                       val copyAttributes: MutableList<Attribute> = arrayListOf(),
                       val configurableAttributes: MutableList<ConfigurableAttribute> = arrayListOf(),
                       val media: MutableList<MediaItem> = arrayListOf()
    ) : Serializable

    data class ProductListResponse(
            val hits: List<Product>,
            val categoryName: String,
            val smartFilters: SmartFilters,
            val facets: Facets? = null
    )


    data class ConfigurableOptionSpecificProperty(
            val hex: String = "",
            val swatchImage: String = "",
            val productThumbnail: String = ""
    ) : Serializable

    data class ConfigurableOption(
            val optionId: Int = 0,
            val label: String = "",
            var isInStock: Boolean = false,
            val simpleProductSkus: MutableList<Int> = arrayListOf(),
            val attributeSpecificProperties: ConfigurableOptionSpecificProperty = ConfigurableOptionSpecificProperty()
    ) : Serializable


    data class ConfigurableAttribute(
            val code: ConfigurableCode = ConfigurableCode.COLOR,
            var options: MutableList<ConfigurableOption> = arrayListOf()
    ) : Serializable


    enum class ConfigurableCode(val type: String) {
        @SerializedName("color")
        COLOR("color"),
        @SerializedName("sizeCode")
        SIZECODE("sizeCode")
    }


    data class Attribute(
            val name: String = "",
            val key: String = "",
            val value: String = ""
    ) : Serializable

    data class NameValuePair<T>(
            var value: MutableList<T> = arrayListOf(),
            var name: String = ""
    ) : Serializable


    data class ColorItem(
            val name: String = "",
            val hex: String = "#000000",
            val luminance: Double = 0.0,
            val value: Int = 0
    ) : Serializable

    data class Facets(
            val color: NameValuePair<ColorItem>

    ) : Serializable

    data class MediaItem(
            val position: Int = 0,
            val mediaType: MediaType = MediaType.IMAGE,
            val src: String = "",
            val videoUrl: String = ""

    ) : Serializable


    enum class MediaType(val type: String) {
        @SerializedName("image")
        IMAGE("image"),
        @SerializedName("video")
        VIDEO("video")
    }

    data class Filter(
            val name: String = "",
            val multiSelect: Boolean = false,
            val facetName: String = "",
            val facetValue: String = ""

    )

    data class SmartFilters(
            val selectable: MutableList<Filter> = arrayListOf()
    )


}