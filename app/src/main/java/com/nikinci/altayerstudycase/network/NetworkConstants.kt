package com.nikinci.altayerstudycase.network


object NetworkConstants {


    var BASE_URL = "https://www.nisnass.ae"
    var BASE_URL_IMAGE_FOR_LIST = "https://nis-prod.atgcdn.ae/small_light(p=zoom,of=jpg,q=70)/pub/media/catalog/product/"
    var BASE_URL_IMAGE_FOR_DETAIL = "https://nis-prod.atgcdn.ae/small_light(p=listing2x,of=jpg,q=70)/pub/media/catalog/product/"

    var CONNECT_TIMEOUT_IN_SECONDS: Long = 10
    var READ_TIMEOUT_IN_SECONDS: Long = 60
    var WRITE_TIMEOUT_IN_SECONDS: Long = 60
}