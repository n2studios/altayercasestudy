package com.nikinci.altayerstudycase.network

import com.nikinci.altayerstudycase.BuildConfig
import com.nikinci.altayerstudycase.network.data.Model
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import timber.log.Timber
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit

interface ATService {

    companion object {
        fun create(): ATService {

            val okHttpBuilder = OkHttpClient.Builder()

            okHttpBuilder.connectTimeout(NetworkConstants.CONNECT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                    .readTimeout(NetworkConstants.READ_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                    .writeTimeout(NetworkConstants.WRITE_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)

            val cookieManager = CookieManager()
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)


            if (BuildConfig.LOG_ENABLED) {
                val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Timber.i(message) })
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                okHttpBuilder.addInterceptor(httpLoggingInterceptor)

            }

            val retrofit = Retrofit.Builder()
                    .baseUrl(NetworkConstants.BASE_URL).client(okHttpBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            return retrofit.create(ATService::class.java)
        }
    }


    @GET("/api/women/clothing")
    fun getClothingList(
            @Query("p") p: Int): Observable<Response<Model.ProductListResponse>>

    @GET("/product/findbyslug")
    fun findProductBySlug(
            @Query("slug") slug: String): Observable<Response<Model.Product>>

}