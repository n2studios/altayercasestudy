package com.ttech.android.onlineislem.core

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.nikinci.altayerstudycase.BuildConfig
import com.nikinci.altayerstudycase.network.ATService
import timber.log.Timber.DebugTree
import timber.log.Timber


class ATApplication : MultiDexApplication() {

    init {
        instance = this
    }

    companion object {
        lateinit var instance: ATApplication
            private set

        fun applicationContext(): Context {
            return instance.applicationContext
        }

    }

    val atService by lazy {
        ATService.create()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
            Stetho.initializeWithDefaults(this);
        }
    }
}


